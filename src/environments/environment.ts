// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
        apiKey: 'AIzaSyBWdxRTOQeqnbh63x61TYQ_aS0gnjSoLno',
        authDomain: 'beer-pass.firebaseapp.com',
        databaseURL: 'https://beer-pass.firebaseio.com',
        projectId: 'beer-pass',
        storageBucket: 'beer-pass.appspot.com',
        messagingSenderId: '217383035715',
        appId: '1:217383035715:web:ab8f269d98a523d50305f6',
        measurementId: 'G-NMFDD3HL38'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
