import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class RouteGuard implements CanActivate {
    canActivate() {
        return !!localStorage.getItem('isAuthenticated');
    }
}
