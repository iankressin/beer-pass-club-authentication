import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class RequesterService {

  constructor(
    public db: AngularFirestore,
    public auth: AngularFireAuth
  ) { }

  createServiceRequest(serviceRequest) {
    return this.db.collection('requests').add({
      id: '_' + Math.random().toString(36).substr(2, 9),
      name: serviceRequest.name,
      contact: serviceRequest.contact,
      description: serviceRequest.description,
      serviceType: serviceRequest.serviceType,
      finished: false
    });
  }

  deleteServiceRequest(documentId, value) {
    this.db.collection('requests').doc(documentId).delete();
  }

  getServiceRequests() {
    return this.db.collection('requests').snapshotChanges();
  }

  authentication(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
      .then(res => {
        resolve(res);
      }, err => reject(err));
    });
  }
}
