import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequesterFormComponent } from './requester-form.component';

describe('RequesterFormComponent', () => {
  let component: RequesterFormComponent;
  let fixture: ComponentFixture<RequesterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequesterFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequesterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
