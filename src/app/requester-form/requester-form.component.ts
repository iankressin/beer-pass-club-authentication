import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { RequesterService } from '../services/requester/requester.service';

@Component({
  selector: 'app-requester-form',
  templateUrl: './requester-form.component.html',
  styleUrls: ['./requester-form.component.css']
})
export class RequesterFormComponent  {

  serviceForm = new FormGroup({
    name: new FormControl(),
    contact: new FormControl(),
    description: new FormControl(),
    serviceType: new FormControl()
  });

  shouldShowSuccessMessage = false;

  constructor(
    private formBuilder: FormBuilder,
    public requesterService: RequesterService
  ) {
    this.serviceForm = this.formBuilder.group({
      name: '',
      contact: '',
      description: '',
      serviceType: ''
    });
  }

  onSubmit() {
    this.requesterService.createServiceRequest(this.serviceForm.value);
    this.shouldShowSuccessMessage = true;
    this.serviceForm.reset();
  }
}
