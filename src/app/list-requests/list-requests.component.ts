import { Component, OnInit } from '@angular/core';
import { RequesterService } from '../services/requester/requester.service';
import { services } from '../config/constants.js';

@Component({
  selector: 'app-list-requests',
  templateUrl: './list-requests.component.html',
  styleUrls: ['./list-requests.component.css']
})

export class ListRequestsComponent implements OnInit {

  requests: any;

  constructor(public requesterService: RequesterService) {}

  ngOnInit(): void {
    this.getRequests();
  }

  getRequests() {
    this.requesterService.getServiceRequests().subscribe(result => this.requests = result);
  }

  closeServiceRequest(id) {
    const documentId = id.key.path.segments[6]
    this.requesterService.deleteServiceRequest(documentId, true);
  }

  getServiceVerboseName(id) {
    return services[id]
  }
}
