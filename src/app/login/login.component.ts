import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { RequesterService } from '../services/requester/requester.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  shouldShowErrorMessage = false;

  loginForm = new FormGroup({
    email: new FormControl(),
    password: new FormControl(),
  });

  constructor(
    public requesterService: RequesterService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group({
      email: '',
      password: '',
    });
  }

  doAuth() {
    const email = this.loginForm.value.email;
    const password = this.loginForm.value.password;
    this.requesterService.authentication({email, password})
    .then(
      res => {
        localStorage.setItem('isAuthenticated', 'true');
        this.router.navigate(['/list-requests']);
      },
      err => {
        this.shouldShowErrorMessage = true;
      }
    );
  }
}
