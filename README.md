# App

Desafio para Beer Pass Club

## Rodar projeto localmente

Rodar o comando `ng serve --open` para rodar o servidor de desenvolvimento local e abrir o navegador. O projeto roda em `http://localhost:4200/`.

## Autenticação

Para fazer o login como técnico deve-se usar as seguintes credências: 
	email: tecnico@beerpass.com
	senha: admin123

## Rotas
--`http://localhost:4200/new-service-request` : Criar nova solicitação de serviço

--`http://localhost:4200/list-requests` : Lista todas as solicitações feitas pelos usuários e permite que sejam encerradas